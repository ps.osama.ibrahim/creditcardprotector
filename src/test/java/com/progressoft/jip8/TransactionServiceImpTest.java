package com.progressoft.jip8;

import com.progressoft.jip8.dto.AddTransactionDTO;
import com.progressoft.jip8.dto.ChangeCardDTO;
import com.progressoft.jip8.dto.ProcessTransactionDTO;
import com.progressoft.jip8.model.Transaction;
import com.progressoft.jip8.model.TransactionStatus;
import com.progressoft.jip8.service.CardServiceImp;
import com.progressoft.jip8.exceptions.InvalidCardException;
import com.progressoft.jip8.exceptions.InvalidTransactionException;
import com.progressoft.jip8.service.TransactionServiceImp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import java.util.List;


@SpringBootTest
public class TransactionServiceImpTest {

    @Autowired
    private TransactionServiceImp service;

    @Autowired
    private CardServiceImp cardServiceImp;


    @Test
    public void givenAValidId_whenFetching_returnTransaction() {
        Transaction target = service.saveTransaction(TestDataSource.getTransaction());
        Transaction transaction = service.getTransaction(Long.parseLong("1"));
        Assertions.assertNotNull(transaction);
    }

    @Test
    public void givenInvalidId_whenFetching_returnNull() {
        Assertions.assertNull(service.getTransaction(Long.parseLong("500")));
    }

    @Test
    public void givenValidId_whenChangingStatus_returnSuccess() {
        Transaction target = service.saveTransaction(TestDataSource.getTransaction());
        Assertions.assertNotNull(target);
        Transaction transaction = service.changeTransactionStatus(target.getId(), TransactionStatus.Accepted);
        Assertions.assertEquals(transaction.getStatus(), TransactionStatus.Accepted);
    }

    @Test
    public void givenValidTransaction_whenSaving_transactionSaved() {
        Transaction transaction = TestDataSource.getTransaction();
        Transaction t2 = service.saveTransaction(transaction);
        Assertions.assertNotNull(service.getTransaction(t2.getId()));
    }

    @Test
    public void givenNone_whenGettingAllTransactions_returnListOfTransactions() {
        List<Transaction> target = service.getAllTransactions(2L);
        Assertions.assertEquals(target.size(), 5);
    }

    @Test
    public void givenInvalidId_whenChangingStatus_returnNull() {
        Transaction t1 = service.changeTransactionStatus(Long.parseLong("200"), TransactionStatus.Accepted);
        Assertions.assertNull(t1);
    }

    @Test
    public void givenValidTransactionDTO_whenAddingTransaction_success() {
        AddTransactionDTO dto = new AddTransactionDTO();
        dto.amount = new BigDecimal("100.00");
        dto.merchantId = Long.parseLong("1");
        dto.userId = Long.parseLong("1");
        Transaction transaction = service.addTransaction(dto);
        Assertions.assertNotNull(transaction);
    }

    @Test
    public void givenInvalidMerchantId_whenAddingTransaction_throwException() {
        AddTransactionDTO dto = new AddTransactionDTO();
        dto.amount = new BigDecimal("100.00");
        dto.merchantId = Long.parseLong("3");
        dto.userId = Long.parseLong("1");
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> service.addTransaction(dto));
        Assertions.assertEquals("Merchant does not exist", e.getMessage());
    }

    @Test
    public void givenInvalidUserId_whenAddingTransaction_throwException() {
        AddTransactionDTO dto = new AddTransactionDTO();
        dto.amount = new BigDecimal("100.00");
        dto.merchantId = Long.parseLong("1");
        dto.userId = Long.parseLong("10");
        IllegalArgumentException e = Assertions.assertThrows(IllegalArgumentException.class, () -> service.addTransaction(dto));
        Assertions.assertEquals("User does not exist", e.getMessage());
    }

    @Test
    public void givenValidTransactionProcessDTO_whenProcessing_success() throws InvalidTransactionException {
        ProcessTransactionDTO dto = new ProcessTransactionDTO();
        dto.status = TransactionStatus.Accepted;
        dto.transactionId = Long.parseLong("1");
        dto.cardId = Long.parseLong("1");
        service.processPayment(dto);
        Transaction t = service.getTransaction(dto.transactionId);
        Assertions.assertEquals(TransactionStatus.Accepted, t.getStatus());
    }

    @Test
    public void givenInvalidTransactionProcessDTO_whenProcessing_success() throws InvalidTransactionException {
        ProcessTransactionDTO dto = new ProcessTransactionDTO();
        dto.status = TransactionStatus.Rejected;
        dto.transactionId = Long.parseLong("1");
        dto.cardId = Long.parseLong("1");
        service.processPayment(dto);
        Transaction t = service.getTransaction(dto.transactionId);
        Assertions.assertEquals(TransactionStatus.Rejected, t.getStatus());

    }
    @Test
    public void givenValidTransactionAndCard_whenChangingTransactionCard_success() throws InvalidCardException {
        ChangeCardDTO dto = new ChangeCardDTO();
        dto.cardId=Long.parseLong("3");
        dto.transactionId=Long.parseLong("1");
        service.changeTransactionCard(dto);
        Transaction transaction = service.getTransaction(dto.transactionId);
        Assertions.assertEquals(Long.parseLong("3"),transaction.getCard().getId());

        dto.cardId=Long.parseLong("1");
        dto.transactionId=Long.parseLong("1");
        service.changeTransactionCard(dto);
        transaction = service.getTransaction(dto.transactionId);
        Assertions.assertEquals(Long.parseLong("1"),transaction.getCard().getId());
    }

    @Test
    public void givenInvalidCardID_whenChangingTransactionCard_throwException() {
        ChangeCardDTO dto = new ChangeCardDTO();
        dto.cardId=Long.parseLong("2500");
        dto.transactionId=Long.parseLong("1");
        Assertions.assertThrows(InvalidCardException.class, ()->{
            service.changeTransactionCard(dto);
        });
    }

}
