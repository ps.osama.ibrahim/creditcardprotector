package com.progressoft.jip8;

import com.progressoft.jip8.interfaces.AdminService;
import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class AdminServiceImpTest {
    @Autowired
    AdminService adminService;

    @Test
    public void givenValidLimit_whenGettingAdminCardPageCount_returnValidCount() {
        Assertions.assertEquals(1, adminService.getAdminCardPageCount(5));
    }

    @Test
    public void givenValidLimit_whenGettingAdminUserPageCount_returnValidCount() {
        Assertions.assertEquals(1, adminService.getAdminUserPageCount(5));
    }

    @Test
    public void givenValidLimitAndPage_whenGettingCards_returnValidList(){
        List<Card> cardsAdmin = adminService.getCardsAdmin(5, 1);
        Assertions.assertEquals(3, cardsAdmin.size());
    }

    @Test
    public void givenValidLimitAndPage_whenGettingUsers_returnValidList(){
        List<User> usersAdmin = adminService.getUsersAdmin(5, 1);
        Assertions.assertEquals(3, usersAdmin.size());
    }

    @Test
    public void givenValidUserId_whenBanning_success(){
        User user = adminService.updateUserBanStatus(2L, true);
        Assertions.assertEquals(true, user.isBanned());
        user = adminService.updateUserBanStatus(2L, false);
        Assertions.assertEquals(false, user.isBanned());
    }

    @Test
    public void givenValidCardId_whenBanning_success(){
        Card card = adminService.updateCardBanStatus(3L, true);
        Assertions.assertEquals(true, card.isBanned());
        card = adminService.updateCardBanStatus(3L, false);
        Assertions.assertEquals(false, card.isBanned());
    }

    @Test
    public void givenInvalidCardId_whenBanning_throwException(){
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
            adminService.updateCardBanStatus(20L, true);
        });
    }

    @Test
    public void givenInvalidUserId_whenBanning_throwException(){
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
            adminService.updateUserBanStatus(20L, true);
        });
    }

}
