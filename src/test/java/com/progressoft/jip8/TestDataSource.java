package com.progressoft.jip8;

import com.progressoft.jip8.model.*;

import java.math.BigDecimal;
import java.sql.Date;
import java.time.LocalDateTime;

public class TestDataSource {

    public  static Transaction getTransaction(){
        Transaction transaction = new Transaction();
        transaction.setId(Long.parseLong("1"));
        transaction.setCard(getCard());
        transaction.setMerchant(getMerchant());
        transaction.setAmount(new BigDecimal("120.50"));
        transaction.setCreatedAt(LocalDateTime.parse("2021-03-16T12:22:33.163"));
        transaction.setUpdatedAt(LocalDateTime.parse("2021-03-16T12:22:33.163"));
        transaction.setUser(getNormalUser());
        transaction.setStatus(TransactionStatus.Unidentified);
        return transaction;
    }
    public static Card getCard() {
        Card card = new Card();
        card.setId(Long.parseLong("1"));
        card.setUser(getNormalUser());
        card.setCardNumber(Long.parseLong("1234456789109999"));
        card.setCcv("250");
        card.setExpiryDate(Date.valueOf("2020-03-16"));
        return card;
    }

    public static Role getAdminRole() {
        Role role = new Role();
        role.setName("Admin");
        role.setId(Long.parseLong("1"));
        return role;
    }

    public static Role getUserRole() {
        Role role = new Role();
        role.setName("User");
        role.setId(Long.parseLong("2"));
        return role;
    }

    public static User getNormalUser() {
        User user = getRolelessUser();
        user.setRole(getUserRole());
        return user;
    }

    public static User getAdminUser() {
        User user = getRolelessUser();
        user.setRole(getAdminRole());
        return user;
    }

    public static Merchant getMerchant(){
        Merchant merchant = new Merchant();
        merchant.setBanned(false);
        merchant.setFirstName("TEST");
        merchant.setLastName("TEST");
        merchant.setId(Long.parseLong("1"));
        merchant.setPhone("TEST");
        return merchant;
    }

    private static User getRolelessUser() {
        User user = new User();
        user.setId(Long.parseLong("1"));
        user.setBanned(false);
        user.setEmail("TEST");
        user.setFirstName("TEST");
        user.setLastName("TEST");
        user.setPassword("TEST");
        user.setPhone("TEST");
        user.setCreatedAt(LocalDateTime.parse("2012-09-17T18:47:52.69"));
        user.setUpdatedAt(LocalDateTime.parse("2012-09-17T18:47:52.69"));
        return user;
    }

}
