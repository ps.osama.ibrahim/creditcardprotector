package com.progressoft.jip8;

import com.progressoft.jip8.dto.CreateCardDTO;
import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.User;
import com.progressoft.jip8.service.CardServiceImp;
import com.progressoft.jip8.exceptions.InvalidCardException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;

import java.sql.Date;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


@SpringBootTest
public class CardServiceImpTest{
    @Autowired
    CardServiceImp cardServiceImp;

    @Test
    public void givenValidCardId_whenRetrieving_returnCard() throws InvalidCardException {
        Card card = cardServiceImp.getCardById(Long.parseLong("1"));
        Assertions.assertNotNull(card);
    }

    @Test
    public void givenInvalidCardId_whenRetrieving_returnVoid() throws InvalidCardException {
       Assertions.assertThrows(InvalidCardException.class, ()->{
            cardServiceImp.getCardById(Long.parseLong("500"));
        });
    }

    @Test
    public void givenValidUserId_whenRetrievingAllCards_returnCards(){
        List<Card> allCardsByUserId = cardServiceImp.getAllCardsByUserId(Long.parseLong("2"));
        Assertions.assertEquals(2, allCardsByUserId.size());
    }

    @Test
    public void givenInvalidUserId_whenRetrievingAllCards_returnNoCards(){
        List<Card> allCardsByUserId = cardServiceImp.getAllCardsByUserId(Long.parseLong("10"));
        Assertions.assertEquals(0, allCardsByUserId.size());
    }

    @Test
    public void givenValidCardDTO_whenCreating_returnCard() {
        User user = TestDataSource.getNormalUser();
        CreateCardDTO dto = new CreateCardDTO();
        dto.cardNumber = 4031781376470761L;
        dto.ccv = "250";
        dto.expiryDate = Date.valueOf("2200-01-01");
        Card card = cardServiceImp.addCard(dto,user);
        Assertions.assertNotNull(card);
    }


    @Test
    public void givenValidCardId_whenDeleting_deletedCard() {
        cardServiceImp.deleteCard(Long.parseLong("2"));

    }

    @Test
    public void givenInvalidCardId_whenDeleting_throwException() {
        Assertions.assertThrows(EmptyResultDataAccessException.class, () -> {
            cardServiceImp.deleteCard(Long.parseLong("100"));
        });
    }

    @Test
    public void givenValidUserIdAndCardId_whenRetrieving_pass(){
        Card cardByIdAndUserId = cardServiceImp.getCardByIdAndUserId(Long.parseLong("1"), Long.parseLong("2"));
        Assertions.assertNotNull(cardByIdAndUserId);
    }
    @Test
    public void givenInvalidCardId_whenRetrieving_Fail(){
        Card cardByIdAndUserId = cardServiceImp.getCardByIdAndUserId(Long.parseLong("10"), Long.parseLong("1"));
        Assertions.assertNull(cardByIdAndUserId);
    }

    @Test
    public void givenInvalidUserId_whenRetrieving_Fail(){
        Card cardByIdAndUserId = cardServiceImp.getCardByIdAndUserId(Long.parseLong("1"), Long.parseLong("10"));
        Assertions.assertNull(cardByIdAndUserId);
    }

    @Test
    public void givenValidLimit_whenRetrievingPageCount_success(){
        int limit = 10;
        Assertions.assertEquals(1, cardServiceImp.getPageCount(limit));
    }

    @Test
    public void givenValidUserId_whenGettingAllActiveCards_returnActiveCards(){
        Assertions.assertEquals(2, cardServiceImp.getAllCardsByUserId(Long.parseLong("2")).size());
    }

    @Test
    public void givenInvalidUserId_whenGettingAllActiveCards_returnNoCards(){
        Assertions.assertEquals(0, cardServiceImp.getAllCardsByUserId(Long.parseLong("30")).size());
    }

    @Test
    public void givenValidLimitAndPage_whenRetrievingAdminList_returnValidList(){
        Assertions.assertEquals(1, cardServiceImp.getAllCardsAdmin(2,2).size());
    }

    @Test
    public void givenValidCard_whenUpdatingActiveStatus_success() throws InvalidCardException {

        Card card = cardServiceImp.updateCardActiveStatus(1L, false);
        Assertions.assertEquals(false, card.isActive());

        card = cardServiceImp.updateCardActiveStatus(1L, true);
        Assertions.assertEquals(true, card.isActive());
    }

    @Test
    public void givenValidUserId_whenRetrievingActiveCards_returnCards(){
        Assertions.assertEquals(1, cardServiceImp.getActiveCards(2L).size());
    }

    @Test
    public void givenInvalidCardId_whenUpdatingActiveStatus_throwException(){
        Assertions.assertThrows(IllegalArgumentException.class, ()->{
            cardServiceImp.updateCardActiveStatus(30L, true);
        });
    }



    private void setSecurityContext() {
        User applicationUser = mock(User.class);
        Authentication authentication = mock(Authentication.class);
        SecurityContext securityContext = mock(SecurityContext.class);
        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
        when(SecurityContextHolder.getContext().getAuthentication().getName()).thenReturn("test@test.com");
    }

}
