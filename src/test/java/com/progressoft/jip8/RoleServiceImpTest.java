package com.progressoft.jip8;

import com.progressoft.jip8.exceptions.RoleDoesNotExistException;
import com.progressoft.jip8.interfaces.RoleService;
import com.progressoft.jip8.model.Role;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class RoleServiceImpTest {

    @Autowired
    RoleService roleService;

    @Test
    public void givenValidName_whenAdding_success() throws RoleDoesNotExistException {
        Role role1 = roleService.addRole("Role1");
        Assertions.assertNotNull(role1);
        Assertions.assertEquals(role1.getName(), "Role1");
        roleService.removeRole(role1.getId());
    }
    @Test
    public void givenValidId_whenRemoving_success() throws RoleDoesNotExistException {
        Role role1 = roleService.addRole("Role1");
        roleService.removeRole(role1.getId());
    }
    @Test
    public void givenNothing_whenGettingListOfRoles_returnListOfRoles(){
        List<Role> allRoles = roleService.getAllRoles();
        Assertions.assertEquals(2, allRoles.size());
    }
    @Test
    public void givenInvalidRoleId_whenRemoving_throwException(){
        Assertions.assertThrows(RoleDoesNotExistException.class, ()->{
            roleService.removeRole(20L);
        });
    }
}
