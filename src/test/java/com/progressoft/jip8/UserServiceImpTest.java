package com.progressoft.jip8;

import com.progressoft.jip8.dto.CreateUserDTO;
import com.progressoft.jip8.model.User;
import com.progressoft.jip8.service.UserServiceImp;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class UserServiceImpTest {

    @Autowired
    UserServiceImp userServiceImp;

    @Test
    public void givenValidUserId_whenFetching_returnUser() {
        User user = userServiceImp.getUser(Long.parseLong("1"));
        Assertions.assertNotNull(user);
    }

    @Test
    public void givenInvalidUserId_whenFetching_returnUser() {
        User user = userServiceImp.getUser(Long.parseLong("100"));
        Assertions.assertNull(user);
    }

    @Test
    public void givenValidRoleId_whenCreating_ReturnValidUser() {
        CreateUserDTO dto = new CreateUserDTO();
        dto.firstName = "test";
        dto.lastName = "test";
        dto.password = "test";
        dto.phone = "test";
        dto.email="test3@test.com";
        dto.roleId = Long.parseLong("1");
        User user = userServiceImp.createUser(dto);
        System.out.println(user);
        Assertions.assertNotNull(user);
    }

    @Test
    public void givenInvalidRoleId_whenCreating_ReturnValidUser() {
        CreateUserDTO dto = new CreateUserDTO();
        dto.firstName = "test";
        dto.lastName = "test";
        dto.password = "test";
        dto.phone = "test";
        dto.roleId = Long.parseLong("30");
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            userServiceImp.createUser(dto);
        });
    }
    @Test
    public void givenValidEmail_whenSearching_returnUser(){
        User user = userServiceImp.getUserByEmail("test@test.com");
        Assertions.assertNotNull(user);
    }
    @Test
    public void givenInvalidEmail_whenSearching_returnNull(){
        User user = userServiceImp.getUserByEmail("none");
        Assertions.assertNull(user);
    }
}
