insert into ROLE values (1,'Admin');
insert into ROLE values (2,'User');

Insert into USER
( CREATED_AT,  UPDATED_AT,  ROLE_ID ,EMAIL, FIRST_NAME , IS_BANNED,  LAST_NAME , PASSWORD , PHONE ) values
( {ts '2012-09-17 18:47:52.69'},{ts '2012-09-17 18:47:52.69'},1,  'admin@test.com','Admin',FALSE,'admin','$2a$10$KVg9YAFS9rckjeVlrxbF9u8N1Rjclc/ofDCSAqlD4ReuX0PFrfNqW','0796483575');

Insert into USER
( CREATED_AT,  UPDATED_AT,  ROLE_ID ,EMAIL, FIRST_NAME , IS_BANNED,  LAST_NAME , PASSWORD , PHONE ) values
( {ts '2012-09-17 18:47:52.69'},{ts '2012-09-17 18:47:52.69'},2,  'test@test.com','Osama',FALSE,'Ibrahim','$2a$10$KVg9YAFS9rckjeVlrxbF9u8N1Rjclc/ofDCSAqlD4ReuX0PFrfNqW','0796483575');


Insert into Card
( CARD_NUMBER, CCV, EXPIRY_DATE, USER_ID, IS_ACTIVE, IS_BANNED ) values
('4561416685652274','250','2200-01-01',2, TRUE, false);

Insert into Card
( CARD_NUMBER, CCV, EXPIRY_DATE, USER_ID, IS_ACTIVE, IS_BANNED ) values
('4378970717154538','250','2200-01-01',2, TRUE, false);

Insert into Card
( CARD_NUMBER, CCV, EXPIRY_DATE, USER_ID, IS_ACTIVE, IS_BANNED ) values
('4077016123775625','250','2200-01-01',2, false, false);

INSERT INTO MOCK_CREDIT_CARD ( AMOUNT, CARD_HOLDER_NAME ,CARD_NUMBER, CCV ,EXPIRY_DATE ) VALUES
(1000.00, 'Osama Ibrahim', '4561416685652274', '250', '2021-01-01');

INSERT INTO MOCK_CREDIT_CARD ( AMOUNT, CARD_HOLDER_NAME ,CARD_NUMBER, CCV ,EXPIRY_DATE ) VALUES
(100.00, 'Ahmad Ibrahim', '4378970717154538', '250', '2021-01-01');

INSERT INTO MOCK_CREDIT_CARD ( AMOUNT, CARD_HOLDER_NAME ,CARD_NUMBER, CCV ,EXPIRY_DATE ) VALUES
(500.00, 'Noor Ibrahim', '4077016123775625', '250', '2021-01-01');

INSERT INTO MERCHANT
( FIRST_NAME, LAST_NAME, IS_BANNED ,PHONE ) values
('TEST','TEST',FALSE,'TEST');

Insert into TRANSACTION
( AMOUNT  ,CARD_ID,  STATUS,  USER_ID , CREATED_AT  ,UPDATED_AT, MERCHANT_ID ) values
(120.50, null, 'Unidentified', 2, '2021-03-16 12:22:33.163', '2021-03-16 12:22:33.163',1);


Insert into TRANSACTION
( AMOUNT  ,CARD_ID,  STATUS,  USER_ID , CREATED_AT  ,UPDATED_AT, MERCHANT_ID ) values
(120.50, null, 'Accepted', 2, '2021-03-16 12:22:33.163', '2021-03-16 12:22:33.163',1);


Insert into TRANSACTION
( AMOUNT  ,CARD_ID,  STATUS,  USER_ID , CREATED_AT  ,UPDATED_AT, MERCHANT_ID ) values
(120.50, null, 'Rejected', 2, '2021-03-16 12:22:33.163', '2021-03-16 12:22:33.163',1);

Insert into TRANSACTION
( AMOUNT  ,CARD_ID,  STATUS,  USER_ID , CREATED_AT  ,UPDATED_AT, MERCHANT_ID ) values
(75.26, null, 'Unidentified', 2, '2021-03-16 12:22:33.163', '2021-03-16 12:22:33.163',1);

Insert into TRANSACTION
( AMOUNT  ,CARD_ID,  STATUS,  USER_ID , CREATED_AT  ,UPDATED_AT, MERCHANT_ID ) values
(564.99, null, 'Unidentified', 2, '2021-03-16 12:22:33.163', '2021-03-16 12:22:33.163',1);
