package com.progressoft.jip8.contoller;


import com.progressoft.jip8.repo.TransactionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
@CrossOrigin
@Controller
public class MerchantController {

    @Autowired
    TransactionRepo transactionRepo;

    @PostMapping("/api/transaction/:merchant/:amount")
    public ResponseEntity<?> test(){
        return ResponseEntity.ok("works");
    }
}
