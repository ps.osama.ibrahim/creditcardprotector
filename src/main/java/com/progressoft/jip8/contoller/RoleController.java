package com.progressoft.jip8.contoller;


import com.progressoft.jip8.dto.AddRoleDTO;
import com.progressoft.jip8.exceptions.RoleDoesNotExistException;
import com.progressoft.jip8.interfaces.RoleService;
import com.progressoft.jip8.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@Controller
public class RoleController {


    @Autowired
    RoleService roleService;

    @PostMapping("/api/role")
    public ResponseEntity addRole(@RequestBody AddRoleDTO dto){
        Role role = roleService.addRole(dto.name);
        return ResponseEntity.ok(role);
    }

    @GetMapping("/api/role")
    public ResponseEntity<List<Role>> getAllRoles(){
        List<Role> all = roleService.getAllRoles();
        return ResponseEntity.ok(all);
    }
    @DeleteMapping("/api/role")
    public ResponseEntity deleteRole(@RequestBody Long id) {
        try{
            roleService.removeRole(id);
            return ResponseEntity.ok("Role has been deleted successfully");
        }
        catch(RoleDoesNotExistException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
