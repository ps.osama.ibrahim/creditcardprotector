package com.progressoft.jip8.contoller;

import com.progressoft.jip8.dto.AdminCardUpdateBanStatusRequestDTO;
import com.progressoft.jip8.dto.AdminUserUpdateBanStatusRequestDTO;
import com.progressoft.jip8.dto.CardResponseDTO;
import com.progressoft.jip8.dto.UserResponseDTO;
import com.progressoft.jip8.exceptions.IllegalUserException;
import com.progressoft.jip8.interfaces.AdminService;
import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.User;
import com.progressoft.jip8.security.utils.UserExtractor;
import com.progressoft.jip8.utils.ResponseGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class AdminController {

    @Autowired
    UserExtractor userExtractor;

    @Autowired
    AdminService adminService;

    @Autowired
    ResponseGenerator responseGenerator;

    @GetMapping("/api/admin/cards")
    public ResponseEntity getAllCardsAdmin(@RequestParam int limit, @RequestParam int page) {

        try {
            validateAdmin();
            Map<String, Object> res = new HashMap<>();
            res.put("Pages", adminService.getAdminCardPageCount(limit));
            res.put("Cards", convertCardsListToDtoList(adminService.getCardsAdmin(limit,page)));
            return ResponseEntity.ok(res);
        } catch (IllegalUserException e) {
            return ResponseEntity.status(401).body("");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/api/admin/cards")
    public ResponseEntity changeCardBanStatus(@RequestBody AdminCardUpdateBanStatusRequestDTO dto){
        try {
            validateAdmin();
            return ResponseEntity.ok(responseGenerator.generateCardResponse(adminService.updateCardBanStatus(dto.getCardId(), dto.isBanned())));
        } catch (IllegalUserException e) {
            return ResponseEntity.status(401).body("");
        }
    }

    @GetMapping("/api/admin/users")
    public ResponseEntity getAllUsersAdmin(@RequestParam int limit, @RequestParam int page) {
        try {
            validateAdmin();
            Map<String, Object> res = new HashMap<>();
            res.put("Pages", adminService.getAdminUserPageCount(limit));
            res.put("Users", convertUsersListToDtoList(adminService.getUsersAdmin(limit,page)));
            return ResponseEntity.ok(res);
        } catch (IllegalUserException e) {
            return ResponseEntity.status(401).body("");
        }
    }

    @PostMapping("/api/admin/users")
    public ResponseEntity changeUserBanStatus(@RequestBody AdminUserUpdateBanStatusRequestDTO dto){
        try {
            validateAdmin();
            return ResponseEntity.ok(responseGenerator.generateUserResponse(adminService.updateUserBanStatus(dto.getUserId(), dto.isBanned())));
        } catch (IllegalUserException e) {
            return ResponseEntity.status(401).body("");
        }
    }



    private void validateAdmin() throws IllegalUserException {
        User user = userExtractor.getUserFromToken();
        if (!user.getRole().getName().equalsIgnoreCase("Admin"))
            throw new IllegalUserException("Illegal User");
    }

    private List<UserResponseDTO> convertUsersListToDtoList(List<User> list) {
        List<UserResponseDTO> res = new ArrayList<>();
        for (User user : list) {
            res.add(responseGenerator.generateUserResponse(user));
        }
        return res;
    }


    private List<CardResponseDTO> convertCardsListToDtoList(List<Card> list) {
        List<CardResponseDTO> res = new ArrayList<>();
        for (Card card : list) {
            res.add(responseGenerator.generateCardResponse(card));
        }
        return res;
    }
}
