package com.progressoft.jip8.contoller;

import com.progressoft.jip8.dto.CardResponseDTO;
import com.progressoft.jip8.dto.ChangeCardStatusRequestDTO;
import com.progressoft.jip8.dto.CreateCardDTO;
import com.progressoft.jip8.interfaces.CardService;
import com.progressoft.jip8.interfaces.UserService;
import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.User;
import com.progressoft.jip8.security.utils.UserExtractor;
import com.progressoft.jip8.utils.ResponseGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
@CrossOrigin
@Controller
public class CardController {

    @Autowired
    CardService cardService;
    @Autowired
    UserService userService;
    @Autowired
    UserExtractor userExtractor;

    @Autowired
    ResponseGenerator responseGenerator;

    @GetMapping("/api/cards")
    public ResponseEntity getAllCards() {
        User user = userExtractor.getUserFromToken();
        if (Objects.isNull(user))
            return ResponseEntity.badRequest().body("Illegal user");
        List<Card> allCardsByUserId = cardService.getAllCardsByUserId(user.getId());
        return ResponseEntity.ok(cardListToCardResponseList(allCardsByUserId));
    }

    @GetMapping("/api/card")
    public ResponseEntity getCardById(@RequestParam Long number) {
        User user = userExtractor.getUserFromToken();
        Card card = cardService.getCardByIdAndUserId(number, user.getId());
        return ResponseEntity.ok(responseGenerator.generateCardResponse((card)));
    }

    @GetMapping("/api/cards/active")
    public ResponseEntity getAllActiveCards(){
        try{
            User userFromToken = userExtractor.getUserFromToken();
            List<Card> activeCards = cardService.getActiveCards(userFromToken.getId());
            return ResponseEntity.ok(cardListToCardResponseList(activeCards));
        }
        catch(Exception e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PatchMapping("/api/card")
    public ResponseEntity changeCardActiveStatus(@RequestBody ChangeCardStatusRequestDTO dto){

            Card card = cardService.updateCardActiveStatus(dto.getCardId(), dto.isActive());
            return ResponseEntity.ok(responseGenerator.generateCardResponse(card));

    }

    @PostMapping("/api/card")
    public ResponseEntity addCard(@RequestBody CreateCardDTO dto) {

        try {
            User user = userExtractor.getUserFromToken();
            Card card = cardService.addCard(dto, user);
            if (Objects.isNull(card))
                return ResponseEntity.badRequest().body("Illegal Card Arguments");
            return ResponseEntity.ok(responseGenerator.generateCardResponse((card)));
        }
        catch (IllegalArgumentException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }

    }
    private List<CardResponseDTO> cardListToCardResponseList(List<Card> list){
        List<CardResponseDTO> res = new ArrayList<>();
        for(Card c:list){
            res.add(responseGenerator.generateCardResponse(c));
        }
        return res;
    }
}
