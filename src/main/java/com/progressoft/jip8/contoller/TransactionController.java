package com.progressoft.jip8.contoller;

import com.progressoft.jip8.dto.AddTransactionDTO;
import com.progressoft.jip8.dto.ChangeCardDTO;
import com.progressoft.jip8.dto.ProcessTransactionDTO;
import com.progressoft.jip8.exceptions.InvalidCardException;
import com.progressoft.jip8.exceptions.InvalidTransactionException;
import com.progressoft.jip8.interfaces.TransactionService;
import com.progressoft.jip8.model.Transaction;
import com.progressoft.jip8.model.TransactionStatus;
import com.progressoft.jip8.model.User;
import com.progressoft.jip8.security.utils.UserExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Objects;


@Controller
public class TransactionController {

    @Autowired
    TransactionService transactionService;

    @Autowired
    UserExtractor userExtractor;


    @GetMapping("/api/transactions")
    public ResponseEntity getAllTransactions() {
        User user = userExtractor.getUserFromToken();
        List<Transaction> all = transactionService.getAllTransactions(user.getId());
        return ResponseEntity.ok(all);
    }

    @GetMapping("/api/transaction")
    public ResponseEntity getTransactionById(Long id) {

            Transaction transaction = transactionService.getTransaction(id);
            if (Objects.isNull(transaction))
                return ResponseEntity.badRequest().body("Transaction not found");
            return ResponseEntity.ok(transaction);

    }

    @PostMapping("/api/transaction/process")
    public ResponseEntity processTransaction(@RequestBody ProcessTransactionDTO dto) {
        Transaction transaction = transactionService.getTransaction(dto.transactionId);
        if (Objects.isNull(transaction))
            return ResponseEntity.badRequest().body("Invalid Parameters");
        if (transaction.getStatus() != TransactionStatus.Unidentified)
            return ResponseEntity.badRequest().body("Transaction Already Processed");
        try {
            transactionService.processPayment(dto);
            return ResponseEntity.ok("Transaction Successful");
        } catch (InvalidTransactionException e) {
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @PostMapping("/api/transaction")
    public ResponseEntity addTransaction(@RequestBody AddTransactionDTO dto) {
            Transaction transaction = transactionService.addTransaction(dto);
            return ResponseEntity.ok(transaction);
    }

    @PostMapping("/api/transaction/changeCard")
    public ResponseEntity changeTransactionCard(@RequestBody ChangeCardDTO dto){
        try{
            Transaction transaction = transactionService.changeTransactionCard(dto);
            return ResponseEntity.ok(transaction);
        }
        catch(InvalidCardException e){
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }
}
