package com.progressoft.jip8.service;

public interface CreditCardValidator {
    boolean isValid(long number);
}
