package com.progressoft.jip8.service;

import com.progressoft.jip8.dto.AddTransactionDTO;
import com.progressoft.jip8.dto.ChangeCardDTO;
import com.progressoft.jip8.dto.ProcessTransactionDTO;
import com.progressoft.jip8.interfaces.TransactionService;
import com.progressoft.jip8.model.*;
import com.progressoft.jip8.repo.MerchantRepo;
import com.progressoft.jip8.repo.TransactionRepo;
import com.progressoft.jip8.repo.UserRepo;
import com.progressoft.jip8.exceptions.InvalidCardException;
import com.progressoft.jip8.exceptions.InvalidTransactionException;
import com.progressoft.jip8.localPaymentGateway.localPaymentProcessor.PaymentProcessor;
import com.progressoft.jip8.security.utils.UserExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;


import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

@Service
public class TransactionServiceImp implements TransactionService {
    @Autowired
    TransactionRepo transactionRepo;
    @Autowired
    UserRepo userRepo;
    @Autowired
    MerchantRepo merchantRepo;
    @Autowired
    CardServiceImp cardServiceImp;
    @Autowired
    PaymentProcessor paymentProcessor;
    @Autowired
    UserExtractor userExtractor;

    public List<Transaction> getAllTransactions(Long userId) {
        List<Transaction> transactions = transactionRepo.findAllTransactionsByUSerId(userId);
        return transactions;
    }

    public void processPayment(ProcessTransactionDTO dto) throws InvalidTransactionException {

        Transaction transaction = transactionRepo.findById(dto.transactionId).get();
        if(dto.status == TransactionStatus.Rejected){
            transaction.setStatus(TransactionStatus.Rejected);
            transactionRepo.save(transaction);
            return;
        }
        paymentProcessor.processPayment(transaction.getCard().getCardNumber(), transaction.getAmount());
        transaction.setStatus(TransactionStatus.Accepted);
        transactionRepo.save(transaction);
    }

    public Transaction getTransaction(@RequestParam Long id) {
        Optional<Transaction> transaction = transactionRepo.findById(id);
        if (transaction.isPresent())
            return transaction.get();
        return null;

    }

    public Transaction changeTransactionStatus(Long id, TransactionStatus status) {
        Optional<Transaction> target = transactionRepo.findById(id);
        if (!target.isPresent()) {
            return null;
        }
        Transaction targetAfterValidation = target.get();
        targetAfterValidation.setStatus(status);
        return saveTransaction(targetAfterValidation);
    }

    public Transaction changeTransactionCard(ChangeCardDTO dto) throws InvalidCardException {
        Optional<Transaction> target = transactionRepo.findById(dto.transactionId);
        if (!target.isPresent()) {
            return null;
        }
        Transaction targetAfterValidation = target.get();
        Card card = cardServiceImp.getCardById(dto.cardId);
        targetAfterValidation.setCard(card);
        return saveTransaction(targetAfterValidation);
    }

    public Transaction saveTransaction(Transaction transaction) {
        return transactionRepo.save(transaction);
    }

    public Transaction addTransaction(AddTransactionDTO dto) {
        User user = fetchUser(dto.userId);
        Merchant merchant = fetchMerchant(dto.merchantId);
        Transaction transaction = generateTransaction(user, merchant, dto.amount);
        return saveTransaction(transaction);
    }

    private Transaction generateTransaction(User user, Merchant merchant, BigDecimal amount) {
        Transaction transaction = new Transaction();
        transaction.setAmount(amount);
        transaction.setStatus(TransactionStatus.Unidentified);
        transaction.setUser(user);
        transaction.setMerchant(merchant);
        return transaction;
    }

    private User fetchUser(Long userId) {
        Optional<User> user = userRepo.findById(userId);
        if (!user.isPresent())
            throw new IllegalArgumentException("User does not exist");
        return user.get();
    }

    private Merchant fetchMerchant(Long merchantId) {
        Optional<Merchant> merchant = merchantRepo.findById(merchantId);
        if (!merchant.isPresent())
            throw new IllegalArgumentException("Merchant does not exist");
        return merchant.get();
    }



}
