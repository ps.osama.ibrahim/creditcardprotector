package com.progressoft.jip8.service;

import com.progressoft.jip8.dto.CreateUserDTO;
import com.progressoft.jip8.interfaces.UserService;
import com.progressoft.jip8.model.Role;
import com.progressoft.jip8.model.User;
import com.progressoft.jip8.repo.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
public class UserServiceImp implements UserService {
    @Autowired
    UserRepo userRepo;
    @Autowired
    RoleServiceImp roleServiceImp;
    @Autowired
    private PasswordEncoder passwordEncoder;

    public User createUser(CreateUserDTO dto) {
        Role role = roleServiceImp.getRoleById(dto.roleId);
        if (role == null)
            throw new IllegalArgumentException("Role doesn't exist");
        Optional<User> opUser = userRepo.findByEmail(dto.email);
        if (opUser.isPresent())
            throw new IllegalArgumentException("Email already exists");
        User user = generateNewUser(dto, role);
        User save = userRepo.save(user);
        return save;
    }

    public User getUser(Long id) {
        Optional<User> user = userRepo.findById(id);
        if (!user.isPresent())
            return null;
        return user.get();
    }

    public User getUserByEmail(String e) {
        Optional<User> email = userRepo.findByEmail(e);
        if (!email.isPresent())
            return null;
        return email.get();
    }


    private User generateNewUser(CreateUserDTO dto, Role role) {
        User user = new User();
        user.setRole(role);
        user.setCreatedAt(LocalDateTime.now());
        user.setUpdatedAt(LocalDateTime.now());
        user.setPhone(dto.phone);
        user.setPassword((passwordEncoder.encode(dto.password)));
        user.setFirstName(dto.firstName);
        user.setLastName(dto.lastName);
        user.setEmail(dto.email.trim().toLowerCase());
        user.setBanned(false);
        return user;
    }

}
