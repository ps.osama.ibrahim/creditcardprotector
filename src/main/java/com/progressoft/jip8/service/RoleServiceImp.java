package com.progressoft.jip8.service;

import com.progressoft.jip8.exceptions.RoleDoesNotExistException;
import com.progressoft.jip8.interfaces.RoleService;
import com.progressoft.jip8.model.Role;
import com.progressoft.jip8.repo.RoleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class RoleServiceImp implements RoleService {
    @Autowired
    RoleRepo roleRepo;

    public Role getRoleById(Long id) {
        Optional<Role> role = roleRepo.findById(id);
        if (role.isPresent())
            return role.get();
        return null;
    }

    public Role addRole(String name) {
        return roleRepo.save(createRole(name));
    }

    public List<Role> getAllRoles() {
        List<Role> all = roleRepo.findAll();
        return all;
    }

    public void removeRole(Long id) throws RoleDoesNotExistException {
        Optional<Role> byId = roleRepo.findById(id);
        if (!byId.isPresent())
            throw new RoleDoesNotExistException("Role does not exist");
        Role role = byId.get();
        roleRepo.delete(role);
    }

    private Role createRole(String name) {
        Role role = new Role();
        role.setName(name);
        return role;
    }
}
