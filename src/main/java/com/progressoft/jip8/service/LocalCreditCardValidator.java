package com.progressoft.jip8.service;
import org.springframework.stereotype.Service;

@Service
public class LocalCreditCardValidator implements CreditCardValidator {

    /**
     * How to validate credit cards
     *
     * Step 1. Double every second digit from right to left. If doubling of a digit results in a
     * two-digit number, add up the two digits to get a single-digit number (like for 12:1+2, 18=1+8).
     *
     * Step 2. Now add all single-digit numbers from Step 1.
     * 4 + 4 + 8 + 2 + 3 + 1 + 7 + 8 = 37
     *
     * Step 3. Add all digits in the odd places from right to left in the card number.
     * 6 + 6 + 0 + 8 + 0 + 7 + 8 + 3 = 38
     *
     * Step 4. Sum the results from Step 2 and Step 3.
     * 37 + 38 = 75
     *
     * Step 5. If the result from Step 4 is divisible by 10, the card number is valid; otherwise, it is invalid.
     *
     * */

    public boolean isValid(long number)
    {
        return (getSize(number) >= 13 &&
                getSize(number) <= 16) &&
                (prefixMatched(number, 4) ||
                        prefixMatched(number, 5) ||
                        prefixMatched(number, 37) ||
                        prefixMatched(number, 6)) &&
                ((sumOfDoubleEvenPlace(number) +
                        sumOfOddPlace(number)) % 10 == 0);
    }

    private int sumOfDoubleEvenPlace(long number)
    {
        int sum = 0;
        String num = number + "";
        for (int i = getSize(number) - 2; i >= 0; i -= 2)
            sum += getDigit(Integer.parseInt(num.charAt(i) + "") * 2);

        return sum;
    }

    private int getDigit(int number)
    {
        if (number < 9)
            return number;
        return number / 10 + number % 10;
    }

    private int sumOfOddPlace(long number)
    {
        int sum = 0;
        String num = number + "";
        for (int i = getSize(number) - 1; i >= 0; i -= 2)
            sum += Integer.parseInt(num.charAt(i) + "");
        return sum;
    }

    private boolean prefixMatched(long number, int d)
    {
        return getPrefix(number, getSize(d)) == d;
    }

    private int getSize(long d)
    {
        String num = d + "";
        return num.length();
    }


    private long getPrefix(long number, int k)
    {
        if (getSize(number) > k) {
            String num = number + "";
            return Long.parseLong(num.substring(0, k));
        }
        return number;
    }
}