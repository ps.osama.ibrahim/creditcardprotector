package com.progressoft.jip8.service;

import com.progressoft.jip8.dto.CreateCardDTO;
import com.progressoft.jip8.interfaces.CardService;
import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.User;
import com.progressoft.jip8.repo.CardRepo;
import com.progressoft.jip8.security.utils.UserExtractor;
import com.progressoft.jip8.exceptions.InvalidCardException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class CardServiceImp implements CardService {

    @Autowired
    CardRepo cardRepo;

    @Autowired
    UserServiceImp userServiceImp;

    @Autowired
    CreditCardValidator creditCardValidator;

    @Autowired
    UserExtractor userExtractor;

    public Card getCardById(Long id) throws InvalidCardException {
        Optional<Card> card = cardRepo.findById(id);
        if (!card.isPresent())
            throw new InvalidCardException("Card not found");
        return card.get();
    }

    public List<Card> getAllCardsByUserId(Long id) {
        List<Card> allCardsByUserId = cardRepo.findAllCardsByUserId(id);
        return allCardsByUserId;
    }

    public Card getCardByIdAndUserId(Long cardId, Long userId) {
        Optional<Card> cardByIdAndUserId = cardRepo.findCardByIdAndUserId(cardId, userId);
        if (cardByIdAndUserId.isPresent())
            return cardByIdAndUserId.get();
        return null;
    }

    public Card addCard(CreateCardDTO dto, User user) {
        validateCardNumber(dto.cardNumber);
        Card card = generateCard(dto, user);
        Card savedCard = cardRepo.save(card);
        return savedCard;
    }

    public Card updateCardActiveStatus( Long cardId, boolean isActive) {
        Optional<Card> byId = cardRepo.findById(cardId);
        if(!byId.isPresent())
            throw new IllegalArgumentException("Card not found");
        Card card = byId.get();
        card.setActive(isActive);
        return cardRepo.save(card);
    }

    public List<Card> getAllCardsAdmin(int limit, int page){
        int skip = limit * (page-1);
        List<Card> cardsAdmin = cardRepo.getCardsAdmin(skip, limit);
        return cardsAdmin;
    }

    public List<Card> getActiveCards(Long userId){
        List<Card> activeCardsUser = cardRepo.getActiveCardsUser(userId);
        return activeCardsUser;
    }

    public int getPageCount(int limit){
        return cardRepo.getAdminCardPageCount(limit);
    }

    private Card generateCard(CreateCardDTO dto, User user) {
        Card card = new Card();
        card.setExpiryDate(dto.expiryDate);
        card.setCcv(dto.ccv);
        card.setCardNumber(dto.cardNumber);
        card.setUser(user);
        card.setActive(true);
        card.setBanned(false);
        return card;
    }

    private void validateCardNumber(Long cardNumber) {
        if (!creditCardValidator.isValid(cardNumber))
            throw new IllegalArgumentException("Invalid Card");
        if (cardRepo.findCardByNumber(cardNumber).isPresent())
            throw new IllegalArgumentException("Card already Exists");
    }

    public void deleteCard(Long id) {
        cardRepo.deleteById(id);
    }
}
