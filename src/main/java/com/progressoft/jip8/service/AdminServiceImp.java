package com.progressoft.jip8.service;

import com.progressoft.jip8.dto.CardResponseDTO;
import com.progressoft.jip8.dto.UserResponseDTO;
import com.progressoft.jip8.interfaces.AdminService;
import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.User;
import com.progressoft.jip8.repo.CardRepo;
import com.progressoft.jip8.repo.UserRepo;
import com.progressoft.jip8.utils.ResponseGenerator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class AdminServiceImp implements AdminService {

    @Autowired
    CardRepo cardRepo;

    @Autowired
    UserRepo userRepo;

    @Autowired
    ResponseGenerator responseGenerator;


    public List<Card> getCardsAdmin(int limit, int page) {
        int skip = limit * (page - 1);
        List<Card> cardsAdmin = cardRepo.getCardsAdmin(skip, limit);
        return cardsAdmin;
    }

    public int getAdminCardPageCount(int limit)
    {
        return cardRepo.getAdminCardPageCount(limit);
    }
    public int getAdminUserPageCount(int limit){

        return cardRepo.getAdminUserPageCount(limit);
    }


    public List<User> getUsersAdmin(int limit, int page){
        int skip = limit * (page - 1);
        List<User> usersAdmin = userRepo.getUsersAdmin(skip, limit);
        return usersAdmin;
    }

    public Card updateCardBanStatus(Long cardId,boolean banned){
        Optional<Card> byId = cardRepo.findById(cardId);
        if(!byId.isPresent()){
            throw new IllegalArgumentException("Card not Found");
        }
        Card card = byId.get();
        card.setBanned(banned);
        return cardRepo.save(card);
    }

    public User updateUserBanStatus(Long userId,boolean banned){
        Optional<User> byId = userRepo.findById(userId);
        if(!byId.isPresent()){
            throw new IllegalArgumentException("User not Found");
        }
        User user = byId.get();
        user.setBanned(banned);
        return userRepo.save(user);
    }



}
