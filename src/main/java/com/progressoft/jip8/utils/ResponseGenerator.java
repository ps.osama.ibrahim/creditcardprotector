package com.progressoft.jip8.utils;

import com.progressoft.jip8.dto.*;
import com.progressoft.jip8.model.*;
import org.springframework.stereotype.Service;


@Service
public class ResponseGenerator {
    public TransactionResponseDTO generateTransactionResponse(Transaction transaction) {
        TransactionResponseDTO res = new TransactionResponseDTO();
        res.setCreatedAt(transaction.getCreatedAt());
        res.setAmount(transaction.getAmount());
        res.setId(transaction.getId());
        res.setMerchant(generateMerchantResponse(transaction.getMerchant()));
        res.setStatus(transaction.getStatus());
        res.setCard(transaction.getCard() == null ? null : generateCardResponse(transaction.getCard()));
        res.setUserId(transaction.getUser().getId());
        return res;
    }

    public MerchantResponseDTO generateMerchantResponse(Merchant merchant) {
        MerchantResponseDTO res = new MerchantResponseDTO();
        res.setFirstName(merchant.getFirstName());
        res.setLastName(merchant.getLastName());
        res.setId(merchant.getId());
        return res;
    }

    public CardResponseDTO generateCardResponse(Card card) {
        CardResponseDTO res = new CardResponseDTO();
        String cardNumber = card.getCardNumber().toString();
        res.setActive(card.isActive());
        res.setId(card.getId());
        res.setCardNumber("**** **** **** " + cardNumber.substring(cardNumber.length() - 4));
        res.setExpiryDate(card.getExpiryDate());
        res.setBanned(card.isBanned());
        return res;
    }
    public RoleResponseDTO generateRoleResponse(Role role){
        RoleResponseDTO res = new RoleResponseDTO();
        res.setId(role.getId());
        res.setName(role.getName());
        return res;
    }

    public UserResponseDTO generateUserResponse(User user){
        UserResponseDTO res = new UserResponseDTO();
        res.setBanned(user.isBanned());
        res.setEmail(user.getEmail());
        res.setId(user.getId());
        res.setFirstName(user.getFirstName());
        res.setLastName(user.getLastName());
        res.setRole(generateRoleResponse(user.getRole()));
        return res;
    }
}
