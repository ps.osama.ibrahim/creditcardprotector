package com.progressoft.jip8.localPaymentGateway.localPaymentProcessor.repo;


import com.progressoft.jip8.localPaymentGateway.localPaymentProcessor.model.MockCreditCard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MockCreditCardRepo extends JpaRepository<MockCreditCard, Long> {
}
