package com.progressoft.jip8.localPaymentGateway.localPaymentProcessor.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Date;

@Entity
public class MockCreditCard {

    @Id
    private Long cardNumber;

    @Column
    private BigDecimal amount;

    @JsonIgnore
    @Column
    private String ccv;

    @Column
    private Date expiryDate;

    @Column
    private String cardHolderName;

    public Long getCardNumber() {
        return cardNumber;
    }


    public BigDecimal getAmount() {
        return amount;
    }

    public String getCcv() {
        return ccv;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public String getCardHolderName() {
        return cardHolderName;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
