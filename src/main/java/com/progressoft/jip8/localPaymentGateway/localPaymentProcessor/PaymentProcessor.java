package com.progressoft.jip8.localPaymentGateway.localPaymentProcessor;

import com.progressoft.jip8.exceptions.InvalidTransactionException;

import java.math.BigDecimal;

public interface PaymentProcessor {
    //add merchant account number for processing
    void processPayment(Long cardId, BigDecimal amount) throws InvalidTransactionException;
}
