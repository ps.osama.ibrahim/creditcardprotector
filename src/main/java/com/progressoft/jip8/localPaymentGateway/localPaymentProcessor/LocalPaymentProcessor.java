package com.progressoft.jip8.localPaymentGateway.localPaymentProcessor;



import com.progressoft.jip8.exceptions.InvalidTransactionException;
import com.progressoft.jip8.localPaymentGateway.localPaymentProcessor.model.MockCreditCard;
import com.progressoft.jip8.localPaymentGateway.localPaymentProcessor.repo.MockCreditCardRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Optional;


@Service
public class LocalPaymentProcessor implements PaymentProcessor {

    @Autowired
    MockCreditCardRepo mockCreditCardRepo;

    @Override
    public void processPayment(Long cardNumber, BigDecimal amount) throws InvalidTransactionException {
        MockCreditCard card = getCard(cardNumber);
        if (card.getAmount().compareTo(amount) == -1)
            throw new InvalidTransactionException("insufficient funds in card");
        deductPayment(card, amount);
    }

    private void deductPayment(MockCreditCard card, BigDecimal amount) {
        BigDecimal newAmount = card.getAmount().subtract(amount);
        updateCreditCard(card, newAmount);
    }

    private void updateCreditCard(MockCreditCard card, BigDecimal newAmound) {
        card.setAmount(newAmound);
        mockCreditCardRepo.save(card);
    }
    private MockCreditCard getCard(Long cardNumber) throws InvalidTransactionException {
        Optional<MockCreditCard> card = mockCreditCardRepo.findById(cardNumber);
        if(!card.isPresent())
            throw new InvalidTransactionException("Invalid Credit Card");
        return card.get();
    }
}
