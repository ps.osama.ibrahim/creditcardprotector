package com.progressoft.jip8.localPaymentGateway.exceptions;

public class InvaidCreditCardException extends Exception {
    public InvaidCreditCardException(String s) {
        super(s);
    }
}
