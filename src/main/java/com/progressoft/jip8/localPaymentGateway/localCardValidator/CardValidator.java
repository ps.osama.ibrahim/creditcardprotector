package com.progressoft.jip8.localPaymentGateway.localCardValidator;


import com.progressoft.jip8.localPaymentGateway.exceptions.InvaidCreditCardException;

import java.util.Date;

public interface CardValidator {
    void validateCardInfo(Long number, String ccv, Date expireyDate, String name) throws InvaidCreditCardException;
}
