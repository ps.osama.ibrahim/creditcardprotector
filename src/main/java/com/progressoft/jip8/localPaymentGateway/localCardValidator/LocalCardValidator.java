package com.progressoft.jip8.localPaymentGateway.localCardValidator;

import com.progressoft.jip8.localPaymentGateway.localPaymentProcessor.model.MockCreditCard;
import com.progressoft.jip8.localPaymentGateway.localPaymentProcessor.repo.MockCreditCardRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.progressoft.jip8.localPaymentGateway.exceptions.InvaidCreditCardException;
import java.util.Date;
import java.util.Objects;
import java.util.Optional;

@Service
public class LocalCardValidator implements CardValidator {

    @Autowired
    MockCreditCardRepo mockCreditCardRepo;

    @Override
    public void validateCardInfo(Long number, String ccv, Date expiryDate, String name) throws InvaidCreditCardException {
        if (expiryDate.before(new Date()))
            throw new InvaidCreditCardException("Credit Card Expired");

        Optional<MockCreditCard> card = mockCreditCardRepo.findById(number);
        if(!card.isPresent())
            throw new InvaidCreditCardException("Invalid Credit Card");
        MockCreditCard mockCreditCard = card.get();

        if (
                Objects.isNull(mockCreditCard) ||
                        !mockCreditCard.getCcv().equals(ccv) ||
                        !mockCreditCard.getCardHolderName().equals(name) ||
                        !mockCreditCard.getExpiryDate().equals(expiryDate)
        )
            throw new InvaidCreditCardException("Invalid Credit Card");

    }
}
