package com.progressoft.jip8;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.error.ErrorMvcAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@ComponentScan
@SpringBootApplication(exclude = {ErrorMvcAutoConfiguration.class}, scanBasePackages={ "com.progressoft.jip8"})
public class CreditCardProtector {
	public static void main(String[] args) {
		SpringApplication.run(CreditCardProtector.class, args);
	}
}
