package com.progressoft.jip8.exceptions;

public class InvalidTransactionException extends Exception {
    public InvalidTransactionException(String s) {
        super(s);
    }

}
