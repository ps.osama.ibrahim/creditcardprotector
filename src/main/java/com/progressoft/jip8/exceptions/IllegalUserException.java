package com.progressoft.jip8.exceptions;

public class IllegalUserException extends Exception {
    public IllegalUserException(String s) {
        super(s);
    }
}
