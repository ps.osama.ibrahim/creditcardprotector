package com.progressoft.jip8.exceptions;

public class RoleDoesNotExistException extends  Exception {
    public RoleDoesNotExistException(String s) {
        super(s);
    }
}
