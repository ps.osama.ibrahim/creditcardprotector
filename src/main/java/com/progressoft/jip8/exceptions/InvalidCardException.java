package com.progressoft.jip8.exceptions;

public class InvalidCardException extends Exception {
    public InvalidCardException(String s) {
        super(s);
    }
}
