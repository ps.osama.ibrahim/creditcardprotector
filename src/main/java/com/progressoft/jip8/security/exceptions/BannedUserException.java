package com.progressoft.jip8.security.exceptions;

import org.springframework.security.core.AuthenticationException;

public class BannedUserException extends AuthenticationException {
    public BannedUserException(String msg, Throwable t) {
        super(msg, t);
    }

    public BannedUserException(String msg) {
        super(msg);
    }
}
