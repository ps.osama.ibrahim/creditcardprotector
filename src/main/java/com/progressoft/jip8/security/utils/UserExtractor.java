package com.progressoft.jip8.security.utils;


import com.progressoft.jip8.interfaces.UserService;
import com.progressoft.jip8.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

@Service
public class UserExtractor {
    @Autowired
    UserService userService;

    public User getUserFromToken() {
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        return userService.getUserByEmail(email);
    }
}
