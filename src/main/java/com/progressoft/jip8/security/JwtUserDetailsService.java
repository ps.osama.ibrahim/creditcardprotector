package com.progressoft.jip8.security;

import com.progressoft.jip8.interfaces.UserService;
import com.progressoft.jip8.security.exceptions.BannedUserException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;


@Service
public class JwtUserDetailsService implements UserDetailsService {

    @Autowired
    UserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        com.progressoft.jip8.model.User user = userService.getUserByEmail(email);
        if (user == null)
            throw new UsernameNotFoundException("Username not found");
        if (user.isBanned())
            throw new BannedUserException("User is banned");

        return new User(user.getEmail(), user.getPassword(), new ArrayList<>());
    }
}
