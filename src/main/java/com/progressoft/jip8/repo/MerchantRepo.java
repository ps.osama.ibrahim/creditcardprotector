package com.progressoft.jip8.repo;

import com.progressoft.jip8.model.Merchant;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MerchantRepo extends JpaRepository<Merchant,Long> {
}
