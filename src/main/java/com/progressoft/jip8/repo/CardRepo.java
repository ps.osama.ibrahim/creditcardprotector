package com.progressoft.jip8.repo;



import com.progressoft.jip8.model.Card;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;


public interface CardRepo extends JpaRepository<Card,Long> {

    @Query(value ="SELECT * FROM CARD WHERE USER_ID = ?1", nativeQuery = true)
    List<Card> findAllCardsByUserId(Long id);

    @Query(value ="SELECT TOP 1 * FROM CARD WHERE ID = ?1 and USER_ID = ?2", nativeQuery = true)
    Optional<Card> findCardByIdAndUserId(Long cardId, Long userId);

    @Query(value ="SELECT TOP 1 * FROM CARD WHERE CARD_NUMBER = ?1", nativeQuery = true)
    Optional<Card> findCardByNumber(Long cardNumber);

    @Query(value ="SELECT CEILING(COUNT(*) / (?1 + 0.0)) FROM CARD", nativeQuery = true)
    int getAdminCardPageCount(int pageSize);

    @Query(value ="SELECT CEILING(COUNT(*) / (?1 + 0.0)) FROM User", nativeQuery = true)
    int getAdminUserPageCount(int pageSize);

    @Query(value ="SELECT * FROM CARD OFFSET ?1 ROWS FETCH NEXT ?2 ROWS ONLY", nativeQuery = true)
    List<Card> getCardsAdmin(int skip, int take);

    @Query(value ="SELECT * FROM CARD WHERE USER_ID = ?1 and IS_ACTIVE = TRUE and IS_BANNED = false", nativeQuery = true)
    List<Card> getActiveCardsUser(Long userId);

}
