package com.progressoft.jip8.repo;

import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface UserRepo extends JpaRepository<User,Long> {
    @Query(value ="SELECT TOP 1 * FROM USER WHERE EMAIL = ?1", nativeQuery = true)
    Optional<User> findByEmail(String email);


    @Query(value ="SELECT CEILING(COUNT(*) / (?1 + 0.0)) FROM USER", nativeQuery = true)
    int getAdminPageCount(int pageSize);

    @Query(value ="SELECT * FROM USER OFFSET ?1 ROWS FETCH NEXT ?2 ROWS ONLY;", nativeQuery = true)
    List<User> getUsersAdmin(int skip, int take);

}
