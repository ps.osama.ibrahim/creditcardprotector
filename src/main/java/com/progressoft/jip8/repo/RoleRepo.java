package com.progressoft.jip8.repo;


import com.progressoft.jip8.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RoleRepo extends JpaRepository<Role,Long> {
}
