package com.progressoft.jip8.repo;

import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.Transaction;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface TransactionRepo extends JpaRepository<Transaction, Long> {
    @Query(value ="SELECT * FROM TRANSACTION WHERE USER_ID = ?1", nativeQuery = true)
    List<Transaction> findAllTransactionsByUSerId(Long id);
}
