package com.progressoft.jip8.model;

public enum TransactionStatus {
    Accepted, Rejected, Unidentified
}
