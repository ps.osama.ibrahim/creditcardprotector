package com.progressoft.jip8.interfaces;

import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.User;

import java.util.List;
import java.util.Optional;

public interface AdminService {

    List<Card> getCardsAdmin(int limit, int page);
    int getAdminCardPageCount(int limit);
    int getAdminUserPageCount(int limit);
    Card updateCardBanStatus(Long cardId, boolean banned);
    List<User> getUsersAdmin(int limit, int page);
    User updateUserBanStatus(Long userId, boolean banned);
}
