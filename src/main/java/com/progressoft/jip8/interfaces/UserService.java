package com.progressoft.jip8.interfaces;

import com.progressoft.jip8.dto.CreateUserDTO;
import com.progressoft.jip8.model.User;

public interface UserService {
    User getUserByEmail(String email);
    User createUser(CreateUserDTO dto);
}
