package com.progressoft.jip8.interfaces;

import com.progressoft.jip8.exceptions.RoleDoesNotExistException;
import com.progressoft.jip8.model.Role;

import java.util.List;

public interface RoleService {

    Role getRoleById(Long id);
     Role addRole(String name);
     List<Role> getAllRoles();
    void removeRole(Long id) throws RoleDoesNotExistException;
}
