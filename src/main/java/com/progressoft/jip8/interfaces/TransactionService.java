package com.progressoft.jip8.interfaces;

import com.progressoft.jip8.dto.AddTransactionDTO;
import com.progressoft.jip8.dto.ChangeCardDTO;
import com.progressoft.jip8.dto.ProcessTransactionDTO;
import com.progressoft.jip8.model.Transaction;
import com.progressoft.jip8.exceptions.InvalidCardException;
import com.progressoft.jip8.exceptions.InvalidTransactionException;

import java.util.List;

public interface TransactionService {
    List<Transaction> getAllTransactions(Long userId);
    Transaction getTransaction(Long id);
    void processPayment(ProcessTransactionDTO dto) throws InvalidTransactionException;
    Transaction addTransaction(AddTransactionDTO dto);
    Transaction changeTransactionCard(ChangeCardDTO dto) throws InvalidCardException;
}
