package com.progressoft.jip8.interfaces;

import com.progressoft.jip8.dto.ChangeCardStatusRequestDTO;
import com.progressoft.jip8.dto.CreateCardDTO;
import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.User;

import java.util.List;

public interface CardService {
    List<Card> getAllCardsByUserId(Long userId);
    List<Card> getActiveCards(Long userId);
    Card getCardByIdAndUserId(Long number, Long userId);
    Card addCard(CreateCardDTO dto, User user);
    Card updateCardActiveStatus(Long cardId, boolean isActive);

}
