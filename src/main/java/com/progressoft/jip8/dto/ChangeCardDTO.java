package com.progressoft.jip8.dto;
public class ChangeCardDTO {
    public Long cardId;
    public Long transactionId;
}
