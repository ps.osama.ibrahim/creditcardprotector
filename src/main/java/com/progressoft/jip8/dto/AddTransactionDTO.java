package com.progressoft.jip8.dto;

import java.math.BigDecimal;

public class AddTransactionDTO {
    public BigDecimal amount;
    public Long userId;
    public Long merchantId;
}
