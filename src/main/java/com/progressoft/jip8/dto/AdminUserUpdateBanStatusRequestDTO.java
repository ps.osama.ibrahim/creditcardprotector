package com.progressoft.jip8.dto;

public class AdminUserUpdateBanStatusRequestDTO {

    Long userId;
    boolean banned;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }
}
