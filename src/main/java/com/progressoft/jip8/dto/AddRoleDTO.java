package com.progressoft.jip8.dto;

public class AddRoleDTO {
   public String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
