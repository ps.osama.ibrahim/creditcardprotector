package com.progressoft.jip8.dto;

public class CreateUserDTO {
    public String email;
    public String firstName;
    public String lastName;
    public String phone;
    public String password;
    public Long roleId;
}
