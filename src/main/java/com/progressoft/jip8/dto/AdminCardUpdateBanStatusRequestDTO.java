package com.progressoft.jip8.dto;

public class AdminCardUpdateBanStatusRequestDTO {

    Long cardId;
    boolean banned;

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }
}
