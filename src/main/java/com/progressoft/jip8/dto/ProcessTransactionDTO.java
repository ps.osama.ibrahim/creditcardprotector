package com.progressoft.jip8.dto;

import com.progressoft.jip8.model.TransactionStatus;

public class ProcessTransactionDTO {
    public Long transactionId;
    public Long cardId;
    public TransactionStatus status;

}
