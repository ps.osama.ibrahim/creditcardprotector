package com.progressoft.jip8.dto;

public class ChangeCardStatusRequestDTO {
    Long cardId;
    boolean isActive;

    public Long getCardId() {
        return cardId;
    }

    public void setCardId(Long cardId) {
        this.cardId = cardId;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setIsActive(boolean active) {
        isActive = active;
    }
}
