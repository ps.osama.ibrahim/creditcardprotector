package com.progressoft.jip8.dto;

import java.sql.Date;

public class CreateCardDTO {
    public Date expiryDate;
    public String ccv;
    public Long cardNumber;
}
