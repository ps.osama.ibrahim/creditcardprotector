package com.progressoft.jip8.dto;

import com.progressoft.jip8.model.Card;
import com.progressoft.jip8.model.Merchant;
import com.progressoft.jip8.model.TransactionStatus;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

public class TransactionResponseDTO {

    private LocalDateTime createdAt;
    private BigDecimal amount;
    private Long id;
    private MerchantResponseDTO merchant;
    private TransactionStatus status;
    private CardResponseDTO card;
    private Long userId;


    public MerchantResponseDTO getMerchant() {
        return merchant;
    }

    public void setMerchant(MerchantResponseDTO merchant) {
        this.merchant = merchant;
    }

    public CardResponseDTO getCard() {
        return card;
    }

    public void setCard(CardResponseDTO card) {
        this.card = card;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TransactionStatus getStatus() {
        return status;
    }

    public void setStatus(TransactionStatus status) {
        this.status = status;
    }


    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
