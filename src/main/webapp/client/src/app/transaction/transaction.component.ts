import { Component, OnInit, ComponentFactoryResolver } from "@angular/core";
import { TransactionService } from "../transaction.service";
import { Transaction } from "../models/Transaction";
import { CardService } from "../card.service";
import { Card } from "../models/Card";
import { Router } from "@angular/router";

@Component({
  selector: "app-transaction",
  templateUrl: "./transaction.component.html",
  styleUrls: ["./transaction.component.css"]
})
export class TransactionComponent implements OnInit {
  transactions: Transaction[] = [];
  transaction: Transaction;
  cards: Card[] = [];
  error: string = null;
  success: string = null;

  constructor(
    private transactionService: TransactionService,
    private cardService: CardService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.refreshData();
  }
  refreshData() {
    this.transaction = null;
    this.getTransactions();
    this.getCards();
  }

  getTransactions() {
    this.transactionService.getTransactions().subscribe(
      (res: Transaction[]) => {
        this.transactions = res;
        this.error = null;
      },
      err => {
        this.error = err.error;
        this.success = null;
      }
    );
  }
  getCards() {
    this.cardService.getActiveCards().subscribe(
      (res: Card[]) => {
        this.cards = res;
        this.error = null;
      },
      err => {
        this.error = err.error;
        this.success = null;
      }
    );
  }
  setTransaction(t: Transaction) {
    this.transaction = t;
  }
  changeCard(t: Transaction) {
    this.transactions.map(transaction => {
      if (transaction.id == t.id) transaction.card = null;
      return transaction;
    });
  }
  onCardSelect(cardId: Number) {
    let card: Card = this.cards.filter(c => c.id == cardId)[0];

    this.transactionService
      .changeTransactionCard(this.transaction.id, card.id)
      .subscribe(
        (res: Transaction) => {
          this.transaction.card = card;
          this.error = null;
        },
        err => {
          this.error = err.error;
          this.success = null;
        }
      );
  }
  pay() {
    this.transactionService
      .payTransaction(this.transaction.id, this.transaction.card.id)
      .subscribe(
        res => {
          this.success = "Paid Successfuly";
          this.error = null;
        },
        err => {
          this.error = err.error;
          this.success = null;
        },
        () => {
          this.refreshData();
        }
      );
  }
  reject() {
    this.transactionService
      .rejectTransaction(this.transaction.id, this.transaction.card.id)
      .subscribe(
        res => {
          this.success = "Rejected Successfuly";
          this.error = null;
        },
        err => {
          this.error = err.error;
          this.success = null;
        },
        () => {
          this.refreshData();
        }
      );
  }
  onCancel() {
    this.error = null;
    this.success = null;
    this.transaction = null;
  }
}
