import { Injectable } from "@angular/core";
import { HttpClient, HttpParams } from "@angular/common/http";
import { config } from "./config";
@Injectable({
  providedIn: "root"
})
export class CardService {
  constructor(private http: HttpClient) {}

  getCards() {
    return this.http.get(`${config.apiUrl}/cards`);
  }

  getActiveCards() {
    return this.http.get(`${config.apiUrl}/cards/active`);
  }

  updateCard(cardId: number, isActive: boolean) {
    return this.http.patch(`${config.apiUrl}/card`, { cardId, isActive });
  }

  addCard(cardNumber: number, expiryDate: Date, ccv: number) {
    return this.http.post(
      `${config.apiUrl}/card`,
      {
        cardNumber,
        expiryDate,
        ccv
      },
      { responseType: "text" }
    );
  }

  getCardsAdmin(limit: number = 15, page: number = 1) {
    return this.http.get(
      `${config.apiUrl}/admin/cards?limit=${limit}&page=${page}`
    );
  }
  banCard(cardId: number) {
    return this.http.post(
      `${config.apiUrl}/admin/cards`,
      {
        cardId,
        banned: true
      },
      { responseType: "text" }
    );
  }
  unBanCard(cardId: number) {
    return this.http.post(
      `${config.apiUrl}/admin/cards`,
      {
        cardId,
        banned: false
      },
      { responseType: "text" }
    );
  }
}
