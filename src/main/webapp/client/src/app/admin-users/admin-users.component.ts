import { Component, OnInit } from "@angular/core";
import { User } from "../models/User";
import { AuthserviceService } from "../authservice.service";

@Component({
  selector: "app-admin-users",
  templateUrl: "./admin-users.component.html",
  styleUrls: ["./admin-users.component.css"]
})
export class AdminUsersComponent implements OnInit {
  users: User[] = [];
  user: User = null;
  error: string = null;
  success: string = null;
  currentPage: number = 1;
  pageSize: number = 5;
  pages: number = 0;
  search: boolean = false;

  constructor(private authService: AuthserviceService) {}

  ngOnInit(): void {
    this.getAllUsers();
  }

  getAllUsers() {
    this.authService.getUsersAdmin(this.pageSize, this.currentPage).subscribe(
      (res: any) => {
        this.users = res.Users;
        this.pages = res.Pages;
      },
      err => {
        this.error = err.error;
        this.success = null;
      }
    );
  }
  banUser(user: User) {
    this.authService.banUser(user.id).subscribe(
      res => {
        this.success = `User ${user.id} Banned`;
        this.error = null;
        this.getAllUsers();
      },
      err => {
        this.success = null;
        this.error = err.error;
      }
    );
  }

  unBanUser(user: User) {
    this.authService.unBanUser(user.id).subscribe(
      res => {
        this.success = `User ${user.id} Unbanned`;
        this.error = null;
        this.getAllUsers();
      },
      err => {
        this.success = null;
        this.error = err.error;
      }
    );
  }
  onNext() {
    this.currentPage++;
    this.getAllUsers();
  }
  onPrevious() {
    this.currentPage--;
    this.getAllUsers();
  }
}
