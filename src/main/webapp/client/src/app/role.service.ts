import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { config } from "./config";

@Injectable({
  providedIn: "root"
})
export class RoleService {
  constructor(private http: HttpClient) {}

  getRoles() {
    return this.http.get(`${config.apiUrl}/role`);
  }
  addRole(name: string) {
    return this.http.post(
      `${config.apiUrl}/role`,
      {
        name
      },
      { responseType: "text" }
    );
  }
}
