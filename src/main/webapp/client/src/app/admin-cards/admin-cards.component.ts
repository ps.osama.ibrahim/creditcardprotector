import { Component, OnInit } from "@angular/core";
import { CardService } from "../card.service";
import { Card } from "../models/Card";

@Component({
  selector: "app-admin-cards",
  templateUrl: "./admin-cards.component.html",
  styleUrls: ["./admin-cards.component.css"]
})
export class AdminCardsComponent implements OnInit {
  cards: Card[] = [];
  pages: number = 0;
  currentPage: number = 1;
  pageSize: number = 2;
  search: boolean = false;
  searchTerm: number = null;
  error: string = null;
  success: string = null;

  constructor(private cardService: CardService) {}

  ngOnInit(): void {
    this.getAllCards();
  }

  onSearch() {
    this.search = true;
  }

  getAllCards() {
    this.cardService.getCardsAdmin(this.pageSize, this.currentPage).subscribe(
      (res: any) => {
        this.cards = res.Cards;
        this.pages = res.Pages;
      },
      err => {
        this.error = err.error;
        this.success = null;
      }
    );
  }
  banCard(card: Card) {
    this.cardService.banCard(card.id).subscribe(
      res => {
        this.success = `Card ${card.cardNumber} Banned`;
        this.error = null;
        this.getAllCards();
      },
      err => {
        this.success = null;
        this.error = err.error;
      }
    );
  }
  unBanCard(card: Card) {
    this.cardService.unBanCard(card.id).subscribe(
      res => {
        this.success = `Card ${card.cardNumber} Unbanned`;
        this.error = null;
        this.getAllCards();
      },
      err => {
        this.success = null;
        this.error = err.error;
      }
    );
  }
  onNext() {
    this.currentPage++;
    this.getAllCards();
  }
  onPrevious() {
    this.currentPage--;
    this.getAllCards();
  }
}
