import { Role } from "./Role";

export class User {
  id: number;
  firstName: string;
  lastName: string;
  email: string;
  phone: string;
  role: Role;
  banned: boolean;
}
