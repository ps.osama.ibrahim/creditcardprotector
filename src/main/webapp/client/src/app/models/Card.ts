export class Card {
  id: number;
  expiryDate: Date;
  cardNumber: number;
  active: boolean;
  banned: boolean;
}
