export enum TransactionStatus {
  Accepted = "Accepted",
  Unidentified = "Unidentified",
  Rejected = "Rejected"
}
