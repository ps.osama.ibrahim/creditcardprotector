export class Merchant {
  id: number;
  firstName: string;
  lastName: string;
  phone: string;
  banned: boolean;
}
