import { Card } from "./Card";
import { User } from "./User";
import { Merchant } from "./Merchant";
import { TransactionStatus } from "./TransactionStatus";

export class Transaction {
  id: number;
  amount: number;
  status: TransactionStatus;
  card: Card;
  user: User;
  merchant: Merchant;
  createdAt: Date;
  updatedAt: Date;
}
