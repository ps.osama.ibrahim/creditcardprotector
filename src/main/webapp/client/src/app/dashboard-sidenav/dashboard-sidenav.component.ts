import { Component, OnInit } from "@angular/core";
import { AuthserviceService } from "../authservice.service";

@Component({
  selector: "app-dashboard-sidenav",
  templateUrl: "./dashboard-sidenav.component.html",
  styleUrls: ["./dashboard-sidenav.component.css"]
})
export class DashboardSidenavComponent implements OnInit {
  constructor(private authService: AuthserviceService) {}

  ngOnInit(): void {}
  isAdmin() {
    return (
      JSON.parse(atob(this.authService.getJwtToken().split(".")[1])).role ==
      "Admin"
    );
  }
}
