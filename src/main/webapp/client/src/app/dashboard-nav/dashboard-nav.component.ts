import { Component, OnInit } from "@angular/core";
import { AuthserviceService } from "../authservice.service";
import { Router } from "@angular/router";
@Component({
  selector: "app-dashboard-nav",
  templateUrl: "./dashboard-nav.component.html",
  styleUrls: ["./dashboard-nav.component.css"]
})
export class DashboardNavComponent implements OnInit {
  constructor(
    private authService: AuthserviceService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  signout() {
    this.authService.logout();
    this.router.navigate(["/login"]);
  }
}
