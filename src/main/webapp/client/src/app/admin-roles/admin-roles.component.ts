import { Component, OnInit } from "@angular/core";
import { Role } from "../models/Role";
import { RoleService } from "../role.service";

@Component({
  selector: "app-admin-roles",
  templateUrl: "./admin-roles.component.html",
  styleUrls: ["./admin-roles.component.css"]
})
export class AdminRolesComponent implements OnInit {
  roles: Role[] = [];
  role: Role = null;
  search: boolean = false;
  error: string = null;
  success: string = null;
  add: boolean = false;

  constructor(private roleService: RoleService) {}

  ngOnInit(): void {
    this.getAllRoles();
  }

  deleteRole(role: Role) {}

  addRole(name: string) {
    this.roleService.addRole(name).subscribe(
      res => {
        this.error = null;
        this.success = null;
        this.getAllRoles();
      },
      err => {
        this.error = err.error;
        this.success = null;
      }
    );
  }

  getAllRoles() {
    this.roleService.getRoles().subscribe((res: Role[]) => {
      this.roles = res;
    });
  }

  activateAdd() {
    this.add = true;
  }
  deactivateAdd() {
    this.add = false;
  }
}
