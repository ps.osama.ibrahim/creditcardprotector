import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable, of } from "rxjs";
import { catchError, tap, mapTo } from "rxjs/operators";
import { config } from "./config";

@Injectable({
  providedIn: "root"
})
export class AuthserviceService {
  constructor(private http: HttpClient) {}

  private readonly JWT_TOKEN = "JWT_TOKEN";
  private readonly REFRESH_TOKEN = "REFRESH_TOKEN";
  private loggedUser: string;

  login(user: { username: string; password: string }) {
    return this.http.post(`${config.apiUrl}/auth`, user);
  }

  logout() {
    this.doLogoutUser();
  }

  isLoggedIn() {
    return !!this.getJwtToken();
  }

  getJwtToken() {
    return localStorage.getItem(this.JWT_TOKEN);
  }

  doLoginUser(username: string, jwt: string) {
    this.loggedUser = username;
    this.storeJwtToken(jwt);
  }
  private doLogoutUser() {
    this.loggedUser = null;
    this.removeTokens();
  }
  private storeJwtToken(jwt: string) {
    localStorage.setItem(this.JWT_TOKEN, jwt);
  }

  private removeTokens() {
    localStorage.removeItem(this.JWT_TOKEN);
  }

  createUser(
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    phone: string
  ) {
    return this.http.post(
      `${config.apiUrl}/user`,
      {
        email,
        password,
        firstName,
        lastName,
        phone,
        roleId: 2
      },
      { responseType: "text" }
    );
  }

  getUsersAdmin(limit: number = 15, page: number = 1) {
    return this.http.get(
      `${config.apiUrl}/admin/users?limit=${limit}&page=${page}`
    );
  }
  banUser(userId: number) {
    return this.http.post(
      `${config.apiUrl}/admin/users`,
      {
        userId,
        banned: true
      },
      { responseType: "text" }
    );
  }
  unBanUser(userId: number) {
    return this.http.post(
      `${config.apiUrl}/admin/users`,
      {
        userId,
        banned: false
      },
      { responseType: "text" }
    );
  }
}
