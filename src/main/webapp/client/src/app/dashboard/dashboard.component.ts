import { Component, OnInit } from "@angular/core";
import { AuthserviceService } from "../authservice.service";

@Component({
  selector: "app-dashboard",
  templateUrl: "./dashboard.component.html",
  styleUrls: ["./dashboard.component.css"]
})
export class DashboardComponent implements OnInit {
  name = null;
  token = this.authService.getJwtToken();
  constructor(private authService: AuthserviceService) {}

  ngOnInit(): void {
    this.name = JSON.parse(atob(this.token.split(".")[1])).firstName;
  }
}
