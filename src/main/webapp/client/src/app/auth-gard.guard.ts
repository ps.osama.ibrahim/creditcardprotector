import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthserviceService } from "./authservice.service";

@Injectable({
  providedIn: "root"
})
export class AuthGardGuard implements CanActivate {
  constructor(
    private authService: AuthserviceService,
    private router: Router
  ) {}
  canActivate() {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(["/login"]);
    }
    return this.authService.isLoggedIn();
  }
}
