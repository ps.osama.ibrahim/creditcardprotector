import { Component, OnInit } from "@angular/core";
import { AuthserviceService } from "../authservice.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-create-account",
  templateUrl: "./create-account.component.html",
  styleUrls: ["./create-account.component.css"]
})
export class CreateAccountComponent implements OnInit {
  error: string = null;
  constructor(
    private authService: AuthserviceService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  createAccount(
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    phone: string
  ) {
    if (this.validateEntries(email, password, firstName, lastName, phone))
      this.authService
        .createUser(email, password, firstName, lastName, phone)
        .subscribe(
          res => {
            this.router.navigate(["/login"]);
          },
          err => {
            this.error = err.error;
          }
        );
  }

  private validateEntries(
    email: string,
    password: string,
    firstName: string,
    lastName: string,
    phone: string
  ): boolean {
    if (
      email.length == 0 ||
      password.length == 0 ||
      firstName.length == 0 ||
      lastName.length == 0 ||
      phone.length == 0
    ) {
      this.error = "Invalid form information";
      return false;
    }
    return true;
  }
}
