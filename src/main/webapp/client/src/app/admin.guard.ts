import { Injectable } from "@angular/core";
import {
  CanActivate,
  ActivatedRouteSnapshot,
  RouterStateSnapshot,
  UrlTree,
  Router
} from "@angular/router";
import { Observable } from "rxjs";
import { AuthserviceService } from "./authservice.service";

@Injectable({
  providedIn: "root"
})
export class AdminGuard implements CanActivate {
  token: string;
  constructor(
    private authService: AuthserviceService,
    private router: Router
  ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ):
    | Observable<boolean | UrlTree>
    | Promise<boolean | UrlTree>
    | boolean
    | UrlTree {
    this.token = this.authService.getJwtToken();
    if (!this.token) {
      this.router.navigate(["/login"]);
    }
    return JSON.parse(atob(this.token.split(".")[1])).role == "Admin";
  }
}
