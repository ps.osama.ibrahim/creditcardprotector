import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { LoginComponent } from "./login/login.component";
import { NavbarComponent } from "./navbar/navbar.component";
import { TableComponent } from "./table/table.component";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FormsModule } from "@angular/forms";
import { RouterModule, Routes, Router } from "@angular/router";
import { AuthGardGuard } from "./auth-gard.guard";
import { AdminGuard } from "./admin.guard";
import { DashboardComponent } from "./dashboard/dashboard.component";
import { DashboardNavComponent } from "./dashboard-nav/dashboard-nav.component";
import { TransactionComponent } from "./transaction/transaction.component";
import { CardComponent } from "./card/card.component";
import { DashboardSidenavComponent } from "./dashboard-sidenav/dashboard-sidenav.component";
import { TokenInterceptorInterceptor } from "./token-interceptor.interceptor";
import { CreateAccountComponent } from "./create-account/create-account.component";
import { AdminComponent } from "./admin/admin.component";
import { AdminCardsComponent } from "./admin-cards/admin-cards.component";
import { AdminUsersComponent } from "./admin-users/admin-users.component";
import { AdminRolesComponent } from "./admin-roles/admin-roles.component";
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SearchComponent } from './search/search.component';

const appRoutes: Routes = [
  { path: "login", component: LoginComponent },
  { path: "create", component: CreateAccountComponent },
  {
    path: "dashboard",
    component: DashboardComponent,
    canActivate: [AuthGardGuard],
    children: [
      { path: "transaction", component: TransactionComponent },
      { path: "card", component: CardComponent },
      {
        path: "admin",
        component: AdminComponent,
        canActivate: [AdminGuard]
      },
      {
        path: "admincards",
        component: AdminCardsComponent,
        canActivate: [AdminGuard]
      },
      {
        path: "adminusers",
        component: AdminUsersComponent,
        canActivate: [AdminGuard]
      },
      {
        path: "adminroles",
        component: AdminRolesComponent,
        canActivate: [AdminGuard]
      }
    ]
  },

  { path: "**", redirectTo: "/dashboard" }
];

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    TableComponent,
    DashboardComponent,
    DashboardNavComponent,
    TransactionComponent,
    CardComponent,
    DashboardSidenavComponent,
    CreateAccountComponent,
    AdminCardsComponent,
    AdminUsersComponent,
    AdminRolesComponent,
    FooterComponent,
    HeaderComponent,
    SearchComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    RouterModule.forRoot(appRoutes, { enableTracing: false })
  ],
  exports: [RouterModule],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptorInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
