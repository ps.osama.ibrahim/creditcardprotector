import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { config } from "./config";
import { TransactionStatus } from "./models/TransactionStatus";
@Injectable({
  providedIn: "root"
})
export class TransactionService {
  constructor(private http: HttpClient) {}

  getTransactions() {
    return this.http.get(`${config.apiUrl}/transactions`);
  }
  getTransaction(id: string) {
    return this.http.get(`${config.apiUrl}/transaction`, {
      params: { id }
    });
  }
  payTransaction(transactionId: number, cardId: number) {
    return this.http.post(
      `${config.apiUrl}/transaction/process`,
      {
        transactionId,
        cardId,
        status: TransactionStatus.Accepted
      },
      { responseType: "text" }
    );
  }

  rejectTransaction(transactionId: number, cardId: number) {
    return this.http.post(
      `${config.apiUrl}/transaction/process`,
      {
        transactionId,
        cardId,
        status: TransactionStatus.Rejected
      },
      { responseType: "text" }
    );
  }

  changeTransactionCard(transactionId: number, cardId: number) {
    return this.http.post(`${config.apiUrl}/transaction/changeCard`, {
      transactionId,
      cardId
    });
  }
}
