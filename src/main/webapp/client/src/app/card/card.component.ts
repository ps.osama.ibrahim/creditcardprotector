import { Component, OnInit } from "@angular/core";
import { Card } from "../models/Card";
import { CardService } from "../card.service";

@Component({
  selector: "app-card",
  templateUrl: "./card.component.html",
  styleUrls: ["./card.component.css"]
})
export class CardComponent implements OnInit {
  cards: Card[] = [];
  card: Card = null;
  error: string = null;
  success: string = null;
  addCard: boolean = false;
  constructor(private cardService: CardService) {}

  ngOnInit(): void {
    this.refreshData();
  }

  refreshData() {
    this.card = null;
    this.getAllCards();
  }
  getAllCards() {
    this.cardService.getCards().subscribe(
      (res: Card[]) => {
        this.cards = res;
      },
      err => {
        this.error = err.error;
        this.success = null;
      }
    );
  }
  deactivateCard(card: Card) {
    this.cardService.updateCard(card.id, false).subscribe(
      (res: Card) => {
        this.getAllCards();
      },
      err => {
        this.error = err.error;
        this.success = null;
      }
    );
  }

  activateCard(card: Card) {
    this.cardService.updateCard(card.id, true).subscribe(
      (res: Card) => {
        this.getAllCards();
      },
      err => {
        this.error = err.error;
        this.success = null;
      }
    );
  }
  enableAddCard() {
    this.addCard = true;
  }
  disableAddCard() {
    this.addCard = false;
    this.error = null;
    this.success = null;
  }
  submitCard(cardNumber: number, exp: Date, ccv: number) {
    this.cardService.addCard(cardNumber, exp, ccv).subscribe(
      res => {
        this.addCard = false;
        this.success = "Added Card Successfuly";
        this.error = null;
        this.getAllCards();
      },
      err => {
        this.success = null;
        this.error = err.error;
      }
    );
  }
}
