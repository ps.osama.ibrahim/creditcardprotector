import { Component, OnInit } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { AuthserviceService } from "../authservice.service";
import { Router } from "@angular/router";

@Component({
  selector: "app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.css"]
})
export class LoginComponent implements OnInit {
  error: string;
  loggedin: boolean = false;

  constructor(
    private authService: AuthserviceService,
    private router: Router
  ) {}

  ngOnInit(): void {}
  login(email: string, password: string): void {
    this.authService.login({ username: email, password }).subscribe(
      (res: any) => {
        this.loggedin = true;
        this.authService.doLoginUser(email, res.jwt);
        this.error = null;
        this.router.navigate(["/dashboard"]);
      },
      err => {
        this.error = "Banned User";
      }
    );
  }
  logout() {
    this.authService.logout();
    this.loggedin = false;
    this.router.navigate(["/login"]);
  }
}
